<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css" />
    <title>Practice 8</title>
</head>

<body>
    <div class="header">
        <h1>Tri Bayu Priangga | 21102158 | S1 IF09 MM3</h1>
    </div>

    <div class="row">
        <div class="col-3 col-s-3 menu">
            <ul>
                <li class="lol" href="#home"><a>PHP Tutorial</a></li>
                <a href="php-tutorial/php_syntax.php">
                    <li>PHP Syntax</li>
                </a>
                <a href="php-tutorial/php_comments.php">
                    <li>PHP Comments</li>
                </a>
                <a href="php-tutorial/php_variables.php">
                    <li>PHP Variables</li>
                </a>
                <a href="php-tutorial/php_echo_print.php">
                    <li>PHP Echo and Print</li>
                </a>
                <a href="php-tutorial/php_data_types.php">
                    <li>PHP Data Types</li>
                </a>
                <a href="php-tutorial/php_strings.php">
                    <li>PHP Strings</li>
                </a>
                <a href="php-tutorial/php_numbers.php">
                    <li>PHP Numbers</li>
                </a>
                <a href="php-tutorial/php_math.php">
                    <li>PHP Math</li>
                </a>
                <a href="php-tutorial/php_constants.php">
                    <li>PHP Constants</li>
                </a>
                <a href="php-tutorial/php_magic_constants.php">
                    <li>PHP Magic Constants</li>
                </a>
                <a href="php-tutorial/php_operators.php">
                    <li>PHP Operators</li>
                </a>
                <a href="php-tutorial/php_if_else.php">
                    <li>PHP if...else...elseif Statement
                    </li>
                </a>
                <a href="php-tutorial/php_switch.php">
                    <li>PHP Switch Statement</li>
                </a>
                <a href="php-tutorial/php_loops.php">
                    <li>PHP Loops</li>
                </a>
                <a href="php-tutorial/php_functions.php">
                    <li>PHP Functions</li>
                </a>
                <a href="php-tutorial/php_arrays.php">
                    <li>PHP Arrays</li>
                </a>
                <a href="php-tutorial/php_superglobals.php">
                    <li>PHP Superglobals</li>
                </a>
                <a href="php-tutorial/php_regex.php">
                    <li>PHP RegEx</li>
                </a>
            </ul>
        </div>

        <div class="main col-6 col-s-9">
            <h1 style="text-align: justify" ;>
                <center>PHP</center>
            </h1>
            <p class="style1" style="text-align: justify">
                <strong>Sejarah PHP </strong><br /><br />
                PHP, singkatan dari "Hypertext Preprocessor," adalah bahasa pemrograman server-side yang sering digunakan untuk pengembangan situs web dinamis. Berikut adalah ringkasan sejarah PHP:
                Penciptaan (1994): PHP awalnya dibuat oleh Rasmus Lerdorf pada tahun 1994 sebagai sekumpulan skrip yang disebut "Personal Home Page Tools" untuk mengelola situs web pribadinya. Alat ini menyediakan fungsi dasar, seperti penghitungan pengunjung dan formulir pengiriman email.
                Versi Awal (PHP/FI): Versi awal PHP disebut PHP/FI (Personal Home Page/Forms Interpreter) dan dirilis pada tahun 1995. Ini adalah kumpulan skrip yang dapat menyematkan HTML.
                <br />

                Pengembangan Bersama (1995-1997): Pada tahun 1995, Zeev Suraski dan Andi Gutmans mulai mengembangkan versi baru PHP yang dikenal sebagai PHP 3. Versi ini mengenalkan model pemrograman yang lebih kuat, termasuk dukungan untuk modul ekstensi. Pada tahun 1997, mereka mendirikan Zend Technologies untuk mengelola pengembangan PHP.
                PHP 4 (2000): PHP 4, yang dirilis pada tahun 2000, membawa banyak perubahan signifikan, termasuk pengenalan objek dan peningkatan kinerja. PHP mulai dilihat sebagai bahasa pemrograman server-side yang serius dan mulai digunakan secara luas di seluruh dunia.
                PHP 5 (2004): Dirilis pada tahun 2004, PHP 5 membawa lebih banyak fitur pemrograman berorientasi objek dan peningkatan kinerja. Inti dari PHP 5 ditenagai oleh Zend Engine 2.<br />

                PHP 7 (2015): PHP 7, dirilis pada tahun 2015, adalah loncatan besar dalam hal kinerja. Dibandingkan dengan PHP 5, PHP 7 menghadirkan peningkatan signifikan dalam kecepatan eksekusi dan efisiensi memori. Ini membantu mengurangi beban server dan meningkatkan responsivitas situs web.
                PHP 8 (2020): PHP 8, yang dirilis pada tahun 2020, membawa banyak perubahan dan peningkatan, termasuk Just-In-Time (JIT) compilation untuk meningkatkan kinerja, fitur-fitur baru seperti match expression, attributes, dan banyak perbaikan dan peningkatan lainnya.
                PHP tetap menjadi salah satu bahasa pemrograman server-side yang paling populer di dunia dan terus berkembang dengan pembaruan dan perbaikan keamanan reguler. Ia digunakan secara luas dalam pengembangan web untuk membuat situs web dinamis dan aplikasi web<br />
            </p>
        </div>

        <div class="col-3 col-s-12">
            <div class="aside">
                <h1>Tentang Saya</h1>
                <img class="rounded-image" src="img/profil.png" alt="error" width="150" height="150" />
                <p>Saya Tri Bayu Priangga, Mahasiswa Prodi S1 Teknik Informatika Institut Teknologi Telkom
                    Purwokerto.</p>
            </div>
        </div>
    </div>

    <div class="footer">
        <p>Tri Bayu Priangga © 2023</p>
    </div>
</body>

</html>